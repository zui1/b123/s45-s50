let coursesData = [

	{
		id: "wdc001",
		name: "PHP_laravel",
		description: "Lorem ipsum veniam ut aliqua ut minim deserunt in in dolor elit elit deserunt dolor in officia ex mollit.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Phyton-Django",
		description: "Lorem ipsum veniam ut aliqua ut minim deserunt in in dolor elit elit deserunt dolor in officia ex mollit. Enim nisi duis occaecat velit minim est ullamco velit ex aliquip minim laborum incididunt minim quis enim do.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum veniam ut aliqua ut minim deserunt in in dolor elit elit deserunt dolor in officia ex mollit. Ullamco sit amet dolor ad dolore eu eu duis in dolore occaecat culpa exercitation occaecat duis voluptate sit ea esse voluptate cupidatat irure ut ea velit proident adipisicing veniam consequat occaecat aute sunt in sunt amet officia nisi.",
		price: 55000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "Nodejs-MERN",
		description: "Lorem ipsum veniam ut aliqua ut minim deserunt in in dolor elit elit deserunt dolor in officia ex mollit. Tempor sint. Eu in cillum et reprehenderit dolor cillum dolor adipisicing in sint ut dolore eiusmod incididunt tempor nostrud consectetur ut aliquip reprehenderit nostrud mollit velit deserunt sed et reprehenderit esse fugiat ullamco dolore incididunt sed eu velit est id proident consectetur dolor nisi quis commodo deserunt eiusmod dolor sit nostrud laborum magna laboris aliqua minim sit commodo proident ut eiusmod et laboris eu consequat elit laboris ex sint sed eiusmod veniam mollit exercitation tempore.",
		price: 45000,
		onOffer: false
	}

]

export default coursesData;