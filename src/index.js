import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

//import bootstrap in index.js, our entry script:
import 'bootstrap/dist/css/bootstrap.min.css'

/*
  Reactjs - syntax used is JSX.

  JSX - Javascript + XML, It is an extension of Javascript that let's us create objects which will be compiled as HTML Elements.

  With JSX, we are able to create JS objects with HTML like syntax.
*/
/*let element = <h1>My First React App!</h1>
console.log(element);*/

/*
  Displaying your react elements:

  ReactDOM.render(<reactElement>,<htmlElementSelectedById>)
*/

//with JSX
//let myName = <h2>Arnon Christopher De La Paz</h2>

//Without JSX
//let myName = React.createElement('h2',{}, "This was not created with JSX Syntax")

/*let person = {

  name: "Stephen Strange",
  age: 45,
  job: "Sorcerer Supreme",
  income: 50000,
  expense: 30000

}*/

/*let sorcererSupreme = <p>My name is {person.name}. I am {person.age} old. I work as {person.job}.</p>

let sorcererSupreme2 = <p>I am {person.name}. I work as {person.job}. My Income is {person.income} .My expense is {person.expense}. My balance is {person.income - person.expense}.</p>*/

/*let sorcererSupreme = (

    <>
    <p>My name is {person.name}. I am {person.age} old. I work as {person.job}.</p>
    <p>I am {person.name}. I work as {person.job}. My Income is {person.income} .My expense is {person.expense}. My balance is {person.income - person.expense}.</p>
    </>

  )*/

/*ReactJS does not like returning 2 elements unless they are wrapped by another element, or what we call a fragment<></>*/

/*
  Components are functions that return react elements.

  Components naming convention: PascalCase

  Components should start in capital letters.
*/

/*const SampleComp = () => {

  return (

        <h1>I am returned by a function.</h1>

    )

}*/

//How are components called?
//Create a self-closing tag <SampleComp /> to call your component.

/*
  In Reactjs, we normally render our components in an entry point or in a mother called App. This is so we can group our components under a single entry point/main component.

  All other components/pages will be contained in our component: <App />

  React.StricMode is a built-in react component which is used to highlight potential problmes in the code and in fact allows to provide more information about errors in our code.
*/


ReactDOM.render(
    <React.StrictMode>
     <App />
    </React.StrictMode>,
    document.getElementById('root'));