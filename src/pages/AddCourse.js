import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../userContext';
import {Redirect, useHistory} from 'react-router-dom';
import Banner from '../components/Banner';

export default function AddCourse() {

    const { user } = useContext(UserContext);

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");

    const [isActive, setIsActive] = useState(false);

      useEffect(() => {

            if (name !== "" && description !== "" && price !== "") {

                setIsActive(true)
                

            } else {

                setIsActive(false)

            }

        }, [name, description, price])

    if (user.isAdmin === true) {

        function createCourse(e) {

            e.preventDefault();

            fetch('http://localhost:4000/courses/', {

                    method: 'POST',
                    headers: {
                        "Content-Type": "application/json",
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price
                    })

                })
                .then(res => res.json())
                .then(data => {

                    console.log(data); 

                        Swal.fire({

                            icon: "success",
                            title: "Course created successfully",
                            text: `Thank you for registering, ${data.email}`

                        })

                        setName("");
   				 		setDescription("");
   				 		setPrice("");

                })
                
        }

        return (
		<>
		<h1 className="my-5">Create Course</h1>
		<Form onSubmit={e => createCourse(e)}>
			<Form.Group>
				<Form.Label>Name:</Form.Label>
				<Form.Control type="text" value={name} onChange={e => {setName(e.target.value)}} placeholder="Enter  Name" required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Description:</Form.Label>
				<Form.Control type="text" value={description} onChange={e => {setDescription(e.target.value)}} placeholder="Enter Description" required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Price:</Form.Label>
				<Form.Control type="number" value={price} onChange={e => {setPrice(e.target.value)}} placeholder="Enter Price" required/>
			</Form.Group>
			{

				isActive
				
				? <Button variant="primary" type="submit">Submit</Button>
				: <Button variant="primary" disabled>Submit</Button>

			}			
		</Form>
		</>

		)

    } else {

        const unAuthorizedUser = {

            title: "Unauthorized Access",
            description: "The page you are trying to access is unavailable",
            buttonCallToAction: "View our courses",
            destination: "/courses"

        }

        return (

            <Banner bannerProp={unAuthorizedUser} />

        )

    }






}