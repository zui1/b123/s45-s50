import React, {useContext, useEffect} from 'react';
import UserContext from '../userContext';
import Banner from '../components/Banner';

export default function Logout(){

	//destructure the returned object of useContext after unwrapping our context:
	const {setUser, unsetUser} = useContext(UserContext);

	/*console.log(setUser);
	console.log(unsetUser);*/

	unsetUser();

	//add empty dependency array to run the useEffect only on initial render.
	useEffect(() => {

		//set the global user state to its initial values.
		setUser({

			id: null,
			isAdmin: null

		})
 // eslint-disable-next-line 
	},[])

	const bannerContent = {

		title: "See You Later!",
		description: "You have logged out of ACDP Booking System",
		buttonCallToAction: "Go Back to Home Page",
		destination: "/"

	}

	return(

		<Banner bannerProp={bannerContent} />

		)

}