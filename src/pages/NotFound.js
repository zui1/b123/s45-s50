import React from 'react';
import Banner from "../components/Banner";


export default function NotFound() {

	let pageNotFound = {
		title: "Page cannot be found.",
		description: "ACDP is one of the well known Booking System in the US.",
		buttonCallToAction: "Go back to Home page",
		destination: "/"
	}
	
    return (

        <>	
    	<Banner bannerProp={pageNotFound}/>
    	</>
    )

}