import React, {useState, useEffect} from 'react';
//import our mock data
//import coursesData from '../data/coursesData';
//import course component
import Course from '../components/Course';

export default function Courses() {

	//create a state with empty array as initial value
	const [coursesArray, setCoursesArray] = useState([]);

	//console.log(coursesData);

	useEffect(() => {

		fetch("http://localhost:4000/courses/getActiveCourses")
		.then(res => res.json())
		.then(data => {

			//resulting new array from mapping the data (which is an array of course documents) will be set into our courseArray state with its setter function.
			setCoursesArray(data.map(course => {

					return (
						//reactjs when creating a group//array of react elements, we need to pass a key prop so that reactjs can recognize each individual instances of the react element. Each key prop should be unique.
						<Course key={course._id} courseProp={course}/>

							)	

			}))

		})

	},[])

	console.log(coursesArray);

	/*
		Each instance of a component is independent from one another.

		So if for example, we called multiple course components, each of those instances are independent from each other. Therefore allowing us to have re-usable UI components.
	*/

	/*let sampleProp1 = "I am sample 1";
	let sampleProp2 = "I am sample 2";*/

	

	//console.log(courseCards);

	return (
		<>
			<h1 className="my-5">Available Courses</h1>
			{coursesArray}
		</>
		)

}