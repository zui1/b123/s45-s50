import React from 'react';
import { Row, Col, Jumbotron } from 'react-bootstrap'

//Link component creates an anchor tag, however, it does not use href and instead it will just commit to switching our page component instead of reloading.
import {Link} from 'react-router-dom'

/*
	Row and Col are components from our react-bootstrap module.
	They create div elements with bootstrap classes.

	react-bootstrap components create react elements with bootstrap classes.
*/

export default function Banner({bannerProp}) {

	//This is now an object
	console.log(bannerProp);

	/*
		Add the title property of bannerProp as your h1,
		Add the description property of bannerProp as p,
		Add the buttonCallToAction property of bannerProp as the text content for your button.
	*/

    return (

        <Row>
			<Col>
				<Jumbotron>
					<h1>{bannerProp.title}</h1>
			 		<p>{bannerProp.description}</p>
			 		<Link to={bannerProp.destination} className="btn btn-primary">{bannerProp.buttonCallToAction}</Link>
				</Jumbotron>
			</Col>
		</Row>

    )

}