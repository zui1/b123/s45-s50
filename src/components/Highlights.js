import React from 'react';
import {Row, Col, Card} from 'react-bootstrap';


export default function Highlights(){

	return (

			<Row>
				<Col xs={12} md={4}>
					<Card className="cardHighlights">
						<Card.Body>
							<Card.Title>
								<h2>Learn from Home</h2>
							</Card.Title>
							<Card.Text>
								Dolor sed magna amet ullamco sed irure do ut laborum laboris nisi ullamco sed eu dolore sed deserunt.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlights">
						<Card.Body>
							<Card.Title>
								<h2>Study Now, Pay Later</h2>
							</Card.Title>
							<Card.Text>
								Dolor sed magna amet ullamco sed irure do ut laborum laboris nisi ullamco sed eu dolore sed deserunt anim sed laborum voluptate exercitation pariatur aute aliqua anim consectetur eu. 
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlights">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of Our Community</h2>
							</Card.Title>
							<Card.Text>
								In reprehenderit aliquip eiusmod sit nulla duis duis esse occaecat. Adipisicing amet anim cillum aliquip quis dolore tempor aute cillum ut.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>

		)

}