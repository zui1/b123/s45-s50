import React, {useContext} from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../userContext';

export default function AppNavBar() {

	/*const {user} = useContext(UserContext);
	console.log(user);*/
	//useContext hook will allow us to "unwrap" our context or use our UserCOntext, get the values passed to it. useContext returns an object after unwrapping our context.
	const {user} = (useContext(UserContext));
	console.log(user)

	/*
		If your component will have children (such as text-content or other react element or componenets), it should have a closing tag. If not, then we stick to our self closing tag.

		classes in reactjs are added as className instead of class.

		"as" prop allows components to be trated as if they are another component. A component
	*/

	return (

		<Navbar bg="primary" expand="lg">
			<Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={Link} to="/">Home</Nav.Link>	
					<Nav.Link as={Link} to="/courses">Courses</Nav.Link>
					{
						user.id
						?
							user.isAdmin
							?
							<>
							<Nav.Link as={Link} to="/addCourse">Add Course</Nav.Link>
							<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							</>
							:
							<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						:
						<>
							<Nav.Link as={Link} to="/register">Register</Nav.Link>	
							<Nav.Link as={Link} to="/login">Login</Nav.Link>
						</>
					}
					
				</Nav>
			</Navbar.Collapse>
		</Navbar>

		)
}